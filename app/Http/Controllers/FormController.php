<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function bio(){
        return view('halaman.form');
    }

    public function finish(Request $request){
        // dd($request->all());
        

        $nama = $request["nama"];
        $last_name = $request["last_name"];
        $gender = $request["gender"];
        $nationality = $request["nationality"];
        $language = $request["language"];
        $bio = $request["bio"];

        return view('halaman.finish', compact('nama','last_name','gender','nationality','language','bio'));
    }
}
