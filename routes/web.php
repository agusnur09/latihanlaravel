<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index');

Route::get('form', 'FormController@bio');

Route::post('finish', 'FormController@finish');

Route::get('/table', function(){
    return view('table.table');
});

Route::get('/data-tables', function(){
    return view('table.data-tables');
});


// CRUD Cast
Route::get('cast/create', 'CastController@create');
Route::post('/cast', 'CastController@tambah');
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');

// CRUD Game
Route::get('game/create', 'GameController@create');
Route::post('game', 'GameController@tambah');
Route::get('/game', 'GameController@index');
Route::get('/game/{game_id}', 'GameController@show');
Route::get('/game/{game_id}/edit', 'GameController@edit');
Route::put('/game/{game_id}', 'GameController@update');
Route::delete('/game/{game_id}', 'GameController@destroy');