@extends('layout.master')

@section('judul')
Halaman Finish
@endsection

@section('content')
    <h1>SELAMAT DATANG {{$nama}} {{$last_name}}!</h1>
    <h3>Terimakasih telah bergabung di website kami. Media belajar kita bersama!</h3>
    <form action="/" method="GET">
        @csrf
        <input type="submit" class="btn btn-success my-1" value="Home">
    </form>
@endsection