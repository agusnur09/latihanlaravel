@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
<form action="/finish" method='post'>
    @csrf
    <p><label for="nama">First Name :</label></p>

    <input type="text" name="nama" id="nama">

    <p><label for="last_name">Last Name :</label></p>

    <input type="text" name="last_name" id="last_name">

    <p><label for="gender">Gender</label></p>
    <input type="radio" name="gender" id="gender" value=male>Male<br>
    <input type="radio" name="gender" id="gender" value="female">Female
    

    <p><label for="nationality">Nationality</label></p>
    <select name="nationality" id="nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="Amerika">Amerika</option>
        <option value="Inggris">Inggris</option>
    </select>

    <p><label for="language">language Spoken</label></p>
    <input type="checkbox" name="language" id="language" value="indonesia">Bahasa Indonesia <br>
    <input type="checkbox" name="language" id="language" value="english">English <br>
    <input type="checkbox" name="language" id="language" value="other">Other

    <p><label>Bio :</label></p>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
<br>
    <input type="submit" value="Submit">
</form>
@endsection