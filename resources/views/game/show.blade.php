@extends('layout.master')

@section('judul')
Detail Game {{$game->name}}
@endsection

@section('content')

<h2>Detail Data Game</h2>

<h1>{{$game->name}}</h1>
<p>Usia : {{$game->gameplay}}</p>
<p>Developer : {{$game->developer}}</p>
<p>Year : {{$game->year}}</p>

@endsection