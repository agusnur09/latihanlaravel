@extends('layout.master')

@section('judul')
Create Data Game
@endsection

@section('content')

<form action="/game" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama Game</label>
        <input type="text" class="form-control" name="name" placeholder="Masukkan Nama Game">
        @error('name')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Gameplay</label>
        <textarea name="gameplay" class="form-control" cols="30" rows="10"></textarea>
        @error('gameplay')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Developer</label>
        <input type="text" class="form-control" name="developer" placeholder="Masukkan Developer">
        @error('developer')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Year</label>
        <input type="text" class="form-control" name="year" placeholder="Masukkan Year">
        @error('year')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>

@endsection