@extends('layout.master')

@section('judul')
Edit Game {{$game->name}}
@endsection

@section('content')

<h2>Edit Data Game</h2>

<form action="/game/{{$game->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama game</label>
        <input type="text" class="form-control" value="{{$game->name}}" name="name" placeholder="Masukkan Nama Game">
        @error('name')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Gameplay</label>
        <textarea name="gameplay" class="form-control" cols="30" rows="10">{{$game->gameplay}}</textarea>
        @error('gameplay')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Developer</label>
        <input type="text" class="form-control" value="{{$game->developer}}" name="developer" placeholder="Masukkan Nama Game">
        @error('developer')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label>Year</label>
        <input type="text" class="form-control" value="{{$game->year}}" name="year" placeholder="Masukkan Nama Game">
        @error('year')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>
@endsection