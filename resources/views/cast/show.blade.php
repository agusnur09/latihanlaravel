@extends('layout.master')

@section('judul')
Detail Cast {{$cast->nama}}
@endsection

@section('content')

<h1>{{$cast->nama}}</h1>
<p>Usia : {{$cast->umur}} Tahun</p>
<p>Bio : {{$cast->bio}}</p>

@endsection